# Arduino Repository

A place to store and share Arduino related projects.

* **bedLeveler** - Simple code to level a platform (bed in this case)
automatically using an accelerometer (MPU6050), Arduino and 4x actuators 
(motorised scissor jacks).