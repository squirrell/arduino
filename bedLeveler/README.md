# User Interface
There are 4 buttons and one error led that make up the user interface. 
Pressing any action button UP, DOWN or LEVEL, while an action is in progress, 
will stop that action.

  *  **UP** - Button to trigger all 4 jacks to move up for a period of time 
to fully extend the jacks
  *  **DOWN** - Button to trigger all 4 jacks to run down for a period of time
to fully lower the jacks
  *  **LEVEL** - Button to run jacks to automatically level the bed
  *  **SET LEVEL** - Button used set the true level of the bed after installing
the MPU6050 on the bed.
  *  **ERROR** - LED to indicate errors.  There are currently 2 error conditions
     *  **1 Blink** - Jacks or jack has run for a period of time that would
  indicate it has reached max extension
     *  **2 Blinks** - Indicates a wrong button was pushed.  For example: if 
        the jacks are fully extended as indicated by a 1 blink error, then the 
        only available button press is the down button.  All other button presses 
        will produce this error

# Wiring
The number of IO pins of the Arduino were almost exhausted during this project.

### MPU6050
The mpu takes 4 connections +5v, Ground, SCL and SDA.  The power is pretty 
self explanatory with the source being the Arduino.

*  **SCL** Is the clock pin and connects to the SCL pin on the Arduino (pin A5)
*  **SDA** Is the data pin and connects to the SDA pin on the Arduino (pin A4)

### Buttons (input)
All button pins use the internal pull up feature of the Arduino, so they can 
be directly connected to ground after passing through a button. (no special 
pull up or pull down wiring required)

```
int allUp = A0; // Button to move all jacks up
int allDown = 2; // Button to move all jacks down
int leveler = 12;  // Button to automatically level bed
int levelSet = 11; // Button to set level
```
### Error
The led pin flashes +5v as required. To protect the LED (and the Arduino)
use a typical led resistor in series with the LED and ground.  In this a 
330 ohm resistor.

```
int ERR = 13;  // Error indicator LED
```

### Actuators
These output pins connect to the relay board.  They are typically low 
(gnd / 0v) but are set high (+5v) when the jack is expected to be extended 
higher (Higher) or lower (Down).  The Head indicates the head of the bed 
and is the opposite to Toe.  Right is the right hand side of the bed when
looking down at the bed, not laying down (looking up).

The jacks are then wired into the relays.

Jack pin outs
```
int HRU = 10; // HeadRightUp
int HRD = 9;  // HeadRightDown
int HLU = 8;  // HeadLeftUp
int HLD = 7;  // HeadLeftDown
int TLU = 6;  // ToeLeftUp
int TLD = 5;  // ToeLeftDown
int TRU = 4;  // ToeRightUp
int TRD = 3;  // ToeRightDown
```

# Software
### Special Variables
There are a number of variables that behave like constants and are used 
to set up the environment the software runs in.

* Toggle writing to the serial output which is seen via the serial monitor

    ```bool debugOn = true;```

* The delay between repeating error flashes (milliseconds)

    ``` int flashErrors = 2000;```

* Average reads when setting a level bed

    ```int avgLevelSetRead = 3000;```

* Average reads when leveling a bed;

    ```int avgLoopRead = 100;```

* How far to move the bed during the orientate setup

    ```int moveBedOrientate = 5000;```

* How far to move the bed leveling bed

    ```int moveBedLevel = 2000;```

* Max Time to run jacks in one direction in milliseconds

    ```long maxTimeJack = 30000;```

* Max time to run jack before indicating that the jack/software is in error (1 blink error above)

    ```long errTimeJack = 45000;```

* Speed of each loop in milliseconds (200 is good)

    ```int loopTime = 200;```

* Where to start writing on the eeprom

    ```int eepromBase = 0;```

### Updating the Code

* Install the Arduino ide
  
    https://www.arduino.cc/en/main/software
  
* install git (if not installed)

    https://git-scm.com/downloads

* Clone the repo

    git clone https://bitbucket.org/squirrell/arduino.git

* Install libraries
  
    You can find the library in the lib folder. You have to unzip/extract this library, 
    take the folder named "MPU6050", and paste it inside the Arduino's "library" 
    folder. To do this, go to the location where you have installed Arduino 
    (Arduino --> libraries) and paste it inside the libraries folder. You might
    also have to do the same thing to install the I2Cdev library if you don't 
    already have it for your Arduino.  More details here.
  
    http://www.arduino.cc/en/Guide/Libraries

* Edit, Compile, Push, Test, Repeat (-;
  
    This may take a bit of Google'ing to connect the Arduino to the USB and 
    to open the Serial Monitor for debugging with the correct baud rate.
 


