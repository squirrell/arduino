#include "I2Cdev.h"
#include "MPU6050.h"
#include "EEPROM.h"


// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

//////////////////
// Tunable Values
//////////////////

// Turn on debug (print to serial)
bool debugOn = true;

// Milliseconds between flashing error light;
int flashErrors = 2000; 

// Average reads when setting a level bed;
int avgLevelSetRead = 3000;

// Average reads when leveling a bed;
int avgLoopRead = 100;

// How far to move the bed during the orientate setup
int moveBedOrientate = 5000;

// How far to move the bed leveling bed
int moveBedLevel = 2000;


// Max Time to run jacks in one direction in milliseconds
long maxTimeJack = 30000;
long errTimeJack = 45000;


// Speed of each loop in milliseconds (200 is good)
int loopTime = 200;

// Where to start writing on the eeprom
int eepromBase = 0;

//////////////////

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 accelgyro;
//MPU6050 accelgyro(0x69); // <-- use for AD0 high

int16_t ax, ay, az;
int16_t avgx, avgy, avgz;
int16_t lvlx, lvly, lvlz;

int16_t mpuOrient[2][3];

char inS, inB, state, prev;
bool actLeveler;

int allUp = A0; // Button to move all jacks up
int allDown = 2; // Button to move all jacks down
int ERR = 13;  // Error indicator LED
int leveler = 12;  // Button to automatically level bed
int levelSet = 11; // Button to set level
int HRU = 10; // HeadRightUp
int HRD = 9;  // HeadRightDown
int HLU = 8;  // HeadLeftUp
int HLD = 7;  // HeadLeftDown
int TLU = 6;  // ToeLeftUp
int TLD = 5;  // ToeLeftDown
int TRU = 4;  // ToeRightUp
int TRD = 3;  // ToeRightDown

bool mvBedOn;
char mvSide, mvDirect, allowDirect;
long prevLoop, mvEndTime, mvPrevTimeCheck;
int btnCount, error;

long mvLeftHeadJackTime, mvRightHeadJackTime, mvLeftTailJackTime, mvRightTailJackTime;

// uncomment "OUTPUT_READABLE_ACCELGYRO" if you want to see a tab-separated
// list of the accel X/Y/Z and then gyro X/Y/Z values in decimal. Easy to read,
// not so easy to parse, and slow(er) over UART.
#define OUTPUT_READABLE_ACCELGYRO

// uncomment "OUTPUT_BINARY_ACCELGYRO" to send all 6 axes of data as 16-bit
// binary, one right after the other. This is very fast (as fast as possible
// without compression or data loss), and easy to parse, but impossible to read
// for a human.
//#define OUTPUT_BINARY_ACCELGYRO


void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    // initialize serial communication
    // (9600 chosen because it works straight out of the box with the serial monitor, but
    // it's really up to you depending on your project)
    Serial.begin(9600);

    // initialize device
    Serial.println("Initializing I2C devices...");
    accelgyro.initialize();

    // verify connection
    Serial.println("Testing device connections...");
    Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

    pinMode(levelSet, INPUT_PULLUP); 
    pinMode(leveler, INPUT_PULLUP); 
    pinMode(allUp, INPUT_PULLUP); 
    pinMode(allDown, INPUT_PULLUP);

    pinMode(ERR, OUTPUT); // Declare the LED as an output
    
    pinMode(HRU, OUTPUT); // Declare the LED as an output
    pinMode(HRD, OUTPUT); // Declare the LED as an output
    pinMode(HLU, OUTPUT); // Declare the LED as an output
    pinMode(HLD, OUTPUT); // Declare the LED as an output
    pinMode(TLU, OUTPUT); // Declare the LED as an output
    pinMode(TLD, OUTPUT); // Declare the LED as an output
    pinMode(TRU, OUTPUT); // Declare the LED as an output
    pinMode(TRD, OUTPUT); // Declare the LED as an output

    state = (char) 0;

    // Load settings from EEPORM.
    loadFrmEeprom();

    mvBedOn = false;
    state = (char) 0;
    prev = (char) 0;
    error = 0;
    prevLoop = millis();
}

void loop() {
    //getSerialInput(inS);
    getButtonInput(inB);
    
    // Set state from buttons
    if (inB != (char) 0) {
      if ( inB == prev && state != inB && btnCount >= 3 ){ 
        if (error > 0 && inB == allowDirect) {
          state = inB;
          error = 0;
        } else if ( error == 0 ) {
          state = inB;
        } else {
          error = 2;
        }
      } else if ( inB == prev && state != inB && btnCount < 5 ) { 
        btnCount += 1;
        state = (char) 0;
      } else {
        prev = inB;
      }
    } else {
      prev = (char) 0;
      btnCount = 0;
    }

    // Check jacks not fully extended
    chkBed();
    
    // Do something 
    switch (state) {
      // Setup bed (level / orientate)
      case 's':
        doBedSetup();
        state = (char) 0;
        break;
      // Level bed (flatten)
      case 'f':
        getMPUAvg(avgLoopRead, avgx, avgy, avgz);
        doLeveler();
        break;
      // Bed up
      case 'u':
        doUp();
        break;
      // Bed Down
      case 'd':
        doDown();
        break;
    }

    // Flash the error light
    displError();
    
    if (debugOn) {
      // Print debug to serial
      debug();
    }
    while ( millis() - prevLoop < loopTime ) {
      delay(20);
    }
    prevLoop = millis();
}

void getButtonInput(char &inB) {
    inB = (char) 0;
    if ( ! digitalRead(levelSet) ) {inB = 's';}
    if ( ! digitalRead(leveler) ) {inB = 'f';}
    if ( ! digitalRead(allUp) ) {inB = 'u';}
    if ( ! digitalRead(allDown) ) {inB = 'd';}
    if (debugOn){
      Serial.print("   btn: "); Serial.println(inB);
    }
}

void doBedSetup(){
    int16_t tmpx, tmpy, tmpz;
    setLevel(lvlx, lvly, lvlz);
    // set orientation of accelrometer
    mvBed('l', 'u', moveBedOrientate);
    while (chkBed()) {
      delay(20);
    }
    orientate('l', lvlx, lvly, lvlz);
    getMPUAvg(avgLevelSetRead, avgx, avgy, avgz);
    tmpx = avgx; tmpy = avgy; tmpz = avgz; 
    mvBed('h', 'u', moveBedOrientate);
    while (chkBed()) {
      delay(20);
    }
    orientate('t', tmpx, tmpy, tmpz);
    // Save these settings to EEPORM so
    // They are available on power up.
    saveToEeprom();
}

void saveToEeprom(){
    int storageSize = 2; //bytes
    int base = eepromBase;
    
    eeprom_write_block(static_cast<void *>(&lvlx), base, storageSize);
    base += storageSize;
    eeprom_write_block(static_cast<void *>(&lvly), base, storageSize);
    base += storageSize;
    eeprom_write_block(static_cast<void *>(&lvlz), base, storageSize);
    base += storageSize;
    eeprom_write_block(static_cast<void *>(&mpuOrient[0][0]), base, storageSize);
    base += storageSize;
    eeprom_write_block(static_cast<void *>(&mpuOrient[0][1]), base, storageSize);
    base += storageSize;
    eeprom_write_block(static_cast<void *>(&mpuOrient[0][2]), base, storageSize);
    base += storageSize;
    eeprom_write_block(static_cast<void *>(&mpuOrient[1][0]), base, storageSize);
    base += storageSize;
    eeprom_write_block(static_cast<void *>(&mpuOrient[1][1]), base, storageSize);
    base += storageSize;
    eeprom_write_block(static_cast<void *>(&mpuOrient[1][2]), base, storageSize);
    base += storageSize;
}

void loadFrmEeprom(){
  int storageSize = 2; //bytes
  int base = eepromBase;

  eeprom_read_block(static_cast<void *>(&lvlx),base,storageSize);
  base += storageSize;
  eeprom_read_block(static_cast<void *>(&lvly),base,storageSize);
  base += storageSize;
  eeprom_read_block(static_cast<void *>(&lvlz),base,storageSize);
  base += storageSize;
  eeprom_read_block(static_cast<void *>(&mpuOrient[0][0]),base,storageSize);
  base += storageSize;
  eeprom_read_block(static_cast<void *>(&mpuOrient[0][1]),base,storageSize);
  base += storageSize;
  eeprom_read_block(static_cast<void *>(&mpuOrient[0][2]),base,storageSize);
  base += storageSize;
  eeprom_read_block(static_cast<void *>(&mpuOrient[1][0]),base,storageSize);
  base += storageSize;
  eeprom_read_block(static_cast<void *>(&mpuOrient[1][1]),base,storageSize);
  base += storageSize;
  eeprom_read_block(static_cast<void *>(&mpuOrient[1][2]),base,storageSize);
  base += storageSize;
}

void setLevel(int16_t &lvlx, int16_t &lvly, int16_t &lvlz){
    digitalWrite(HRU, true);
    digitalWrite(HRD, true);
    digitalWrite(HLU, true);
    digitalWrite(HLD, true);
    digitalWrite(TLU, true);
    digitalWrite(TLD, true);
    digitalWrite(TRU, true);
    digitalWrite(TRD, true);
    getMPUAvg(avgLevelSetRead, avgx, avgy, avgz);
    lvlx = avgx; lvly = avgy; lvlz  = avgz;
    digitalWrite(HRU, false);
    digitalWrite(HRD, false);
    digitalWrite(HLU, false);
    digitalWrite(HLD, false);
    digitalWrite(TLU, false);
    digitalWrite(TLD, false);
    digitalWrite(TRU, false);
    digitalWrite(TRD, false);
}

void getMPUAvg(int16_t avg, int16_t &avgx, int16_t &avgy, int16_t &avgz) {
    long w_avgx = 0, w_avgy = 0, w_avgz = 0;
    for (int i=0; i<avg; i++){
      accelgyro.getAcceleration(&ax, &ay, &az);
      w_avgx += ax; w_avgy += ay; w_avgz += az;
      // display tab-separated accel/gyro x/y/z values
//      Serial.print("   RAW: ");
//      Serial.print(ax); Serial.print("-"); Serial.print(w_avgx); Serial.print("\t");
//      Serial.print(ay); Serial.print("\t");
//      Serial.println(az);
    }
    avgx = w_avgx / avg; avgy = w_avgy / avg; avgz = w_avgz / avg;
//    Serial.print("   AVG: ");
//    Serial.print(avgx); Serial.print("\t");
//    Serial.print(avgy); Serial.print("\t");
//    Serial.println(avgz);
}

void orientate(char side, int16_t &avgx, int16_t &avgy, int16_t &avgz) {
    //int max = 0;
    switch (side) {
      case 'l':
        mpuOrient[0][0] = avgx; mpuOrient[0][1] = avgy; mpuOrient[0][2] = avgz;
        break;
      case 'r':
        mpuOrient[0][0] = -avgx; mpuOrient[0][1] = -avgy; mpuOrient[0][2] = -avgz;
        break;
      case 'h':
        mpuOrient[1][0] = -avgx; mpuOrient[1][1] = -avgy; mpuOrient[1][2] = -avgz;
        break;
      case 't':
        mpuOrient[1][0] = avgx; mpuOrient[1][1] = avgy; mpuOrient[1][2] = avgz;
        break;
    }
    if (debugOn) {
      for(int i=0; i<3; i++){
        Serial.println("orient: LR: " + String(mpuOrient[0][i]) + "; HT : " + String(mpuOrient[1][i]));
      }
    }
}

void mvBed(char side, char direct, long duration){
    if (mvBedOn){
      incJackTime();
    }
    mvBedOn = true;
    mvSide = side;
    mvDirect = direct;
    mvEndTime = millis() + duration;
    mvPrevTimeCheck = millis();

    digitalWrite(HRU, false);
    digitalWrite(HRD, false);
    digitalWrite(HLU, false);
    digitalWrite(HLD, false);
    digitalWrite(TLU, false);
    digitalWrite(TLD, false);
    digitalWrite(TRU, false);
    digitalWrite(TRD, false);

    if (mvDirect == 'u') {
      switch (mvSide) {
        case 'l':
          digitalWrite(HLU, true);
          digitalWrite(TLU, true);
          break;
        case 'r':
          digitalWrite(HRU, true);
          digitalWrite(TRU, true);
          break;
        case 'h':
          digitalWrite(HLU, true);
          digitalWrite(HRU, true);
          break;
        case 't':
          digitalWrite(TLU, true);
          digitalWrite(TRU, true);
          break;
        case 'a':
          digitalWrite(HLU, true);
          digitalWrite(HRU, true);
          digitalWrite(TLU, true);
          digitalWrite(TRU, true);
          break;
      }
    } else {
      switch (mvSide) {
        case 'l':
          digitalWrite(HLD, true);
          digitalWrite(TLD, true);
          break;
        case 'r':
          digitalWrite(HRD, true);
          digitalWrite(TRD, true);
          break;
        case 'h':
          digitalWrite(HLD, true);
          digitalWrite(HRD, true);
          break;
        case 't':
          digitalWrite(TLD, true);
          digitalWrite(TRD, true);
          break;
        case 'a':
          digitalWrite(HLD, true);
          digitalWrite(HRD, true);
          digitalWrite(TLD, true);
          digitalWrite(TRD, true);
          break;
      }
    }
}

bool chkBed(){
    if (mvBedOn){
      incJackTime();
      if (abs(mvLeftHeadJackTime) > errTimeJack 
       || abs(mvRightHeadJackTime) > errTimeJack 
       || abs(mvLeftTailJackTime) > errTimeJack 
       || abs(mvRightTailJackTime) > errTimeJack) {
        error = 1;
        state = (char) 0;
        resetJackTime();
        if (mvDirect == 'u') {
          allowDirect = 'd';
        } else {
          allowDirect = 'u';
        }
      }
      if (millis() >= mvEndTime  || error == 1){
        mvBedOn = false;
        digitalWrite(HRU, false);
        digitalWrite(HRD, false);
        digitalWrite(HLU, false);
        digitalWrite(HLD, false);
        digitalWrite(TLU, false);
        digitalWrite(TLD, false);
        digitalWrite(TRU, false);
        digitalWrite(TRD, false);
      }
      return true;
    } else {
      return false;
    }
}

void resetJackTime(){
    long rst;
    if (mvDirect == 'u') {
      rst = maxTimeJack;
    } else {
      rst = - maxTimeJack;
    }
    if (abs(mvLeftHeadJackTime) > maxTimeJack) { mvLeftHeadJackTime = rst;}
    if (abs(mvRightHeadJackTime) > maxTimeJack) { mvRightHeadJackTime = rst;}
    if (abs(mvLeftTailJackTime) > maxTimeJack) { mvLeftTailJackTime = rst;}
    if (abs(mvRightTailJackTime) > maxTimeJack) { mvRightTailJackTime = rst;}
}

void stpBed() {
    if (mvBedOn){
      incJackTime();
      mvBedOn = false;
      digitalWrite(HRU, false);
      digitalWrite(HRD, false);
      digitalWrite(HLU, false);
      digitalWrite(HLD, false);
      digitalWrite(TLU, false);
      digitalWrite(TLD, false);
      digitalWrite(TRU, false);
      digitalWrite(TRD, false);
    }
}

void incJackTime() {
    long now = millis();
    long inc = now - mvPrevTimeCheck;
    mvPrevTimeCheck = now;
    if (mvDirect == 'd') {
      inc = 0 - inc;
    }
    switch (mvSide) {
      case 'l':
        mvLeftHeadJackTime += inc;
        mvLeftTailJackTime += inc;
        break;
      case 'r':
        mvRightHeadJackTime += inc;
        mvRightTailJackTime += inc;
        break;
      case 'h':
        mvLeftHeadJackTime += inc;
        mvRightHeadJackTime += inc;
        break;
      case 't':
        mvLeftTailJackTime += inc;
        mvRightTailJackTime += inc;
        break;
      case 'a':
        mvLeftHeadJackTime += inc;
        mvRightHeadJackTime += inc;
        mvLeftTailJackTime += inc;
        mvRightTailJackTime += inc;
        break;
    }
}

void doLeveler() {
  String action;
  char side, direct;
  int16_t diffMax = -1;
  int16_t diffVal;
  int16_t difx, dify, difz;
  difx = lvlx-avgx; dify = lvly-avgy; difz = lvlz-avgz;
  // find wich x,y,z is biggest and map to 0,1,2 (array index of acellerometer orientaion)
  if ( abs(difx) > abs(dify) ) {
  // x is bigger than y
    diffMax = 0;
    diffVal = difx;
    if ( abs(difz) > abs(difx) ) {
    // z is bigger then x  
      diffMax = 2;
      diffVal = difz;
    }
  } else {
  // y is bigger than x
    diffMax = 1;
    diffVal = dify;
    if ( abs(difz) > abs(dify) ) {
    // z is bigger than y
      diffMax = 2;
      diffVal = difz;
    }
  }
  if (debugOn) {
    Serial.print(" diffMax: "); Serial.print(diffMax);
    Serial.print(" diffVal: "); Serial.println(diffVal);
  }
  //action = "N/A";
  if ( abs(diffVal) < 100 ) {
    action = "NONE";
    stpBed();
  } else {
    if ( abs(mpuOrient[0][diffMax]) > abs(mpuOrient[1][diffMax]) ) {
      // Do Left / Right action first as its larger
      if (mpuOrient[0][diffMax] >= 0) {
        // Left Up is +
        if (diffVal >= 0) { 
          action = "(LD-RU)";
          side = 'r'; direct = 'u';
        } else {
          action = "(RD-LU)";
          side = 'l'; direct = 'u';
        }
      } else {
        // Left Up is -
        if (diffVal < 0) { 
          action = "(LD-RU)";
          side = 'r'; direct = 'u';
        } else {
          action = "(RD-LU)";
          side = 'l'; direct = 'u';
        }
      }
    } else {
      // Do Head / Toe action first as its larger
      if (mpuOrient[1][diffMax] >= 0) {
        // Head Up is +
        if (diffVal >= 0) { 
          action = "(HD-TU)";
          side = 't'; direct = 'u';
        } else {
          action = "(TD-HU)";
          side = 'h'; direct = 'u';
        }
      } else {
        // Head Up is -
        if (diffVal < 0) { 
          action = "(HD-TU)";
          side = 't'; direct = 'u';
        } else {
          action = "(TD-HU)";
          side = 'h'; direct = 'u';
        }
      }
    }
    mvBed(side, direct, moveBedLevel);
  }
  if (debugOn) {
    Serial.println(action);
  }
}

void doUp () {
    mvBed('a', 'u', maxTimeJack);
    state = (char) 0;
}

void doDown () {
    mvBed('a', 'd', maxTimeJack);
    state = (char) 0;
}

void displError(){
    static long prevFlash;
    long now = millis();
    if ( error > 0 ) {
      if (now - prevFlash > flashErrors){
        prevFlash = now;
        for (int i = 0; i < error; i++){
          digitalWrite(ERR, true);
          delay(100);
          digitalWrite(ERR, false);
          delay(100);
        }
      }
    }
}

void debug () {
    Serial.print(" state: "); Serial.println(state);
    
    Serial.print("   LVL: ");
    Serial.print(lvlx); Serial.print("\t");
    Serial.print(lvly); Serial.print("\t");
    Serial.println(lvlz);
    
    Serial.print("   CUR: ");
    Serial.print(avgx); Serial.print("\t");
    Serial.print(avgy); Serial.print("\t");
    Serial.println(avgz);
    
    Serial.print("   DIF: ");
    Serial.print(lvlx-avgx); Serial.print("\t");
    Serial.print(lvly-avgy); Serial.print("\t");
    Serial.println(lvlz-avgz);

    Serial.print("       LH: "); Serial.print(mvLeftHeadJackTime);
    Serial.print("       RH: "); Serial.print(mvRightHeadJackTime);
    Serial.print("       LT: "); Serial.print(mvLeftTailJackTime);
    Serial.print("       RT: "); Serial.print(mvRightTailJackTime);
    Serial.print("      ERR: "); Serial.println(error);
    Serial.println("----------------------------------------------");
}
